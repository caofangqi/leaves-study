# Leaves's Library Of Study Notes 
  定个小目标：一年最低使用一千个小时学习。 
                                2019/06/26 17:04

* [Java](#java)
  * [Java basics](#java-basics)
    * [集合框架](#%E9%9B%86%E5%90%88%E6%A1%86%E6%9E%B6)
    * [io](#io)
    * [类加载器](#%E7%B1%BB%E5%8A%A0%E8%BD%BD%E5%99%A8)
  * [Java 并发](#java-%E5%B9%B6%E5%8F%91)
* [开源项目学习笔记](#%E5%BC%80%E6%BA%90%E9%A1%B9%E7%9B%AE%E5%AD%A6%E4%B9%A0%E7%AC%94%E8%AE%B0)
  * [Tomcat 学习笔记](#tomcat-%E5%AD%A6%E4%B9%A0%E7%AC%94%E8%AE%B0)
* [学习站点记录](#%E5%AD%A6%E4%B9%A0%E7%AB%99%E7%82%B9%E8%AE%B0%E5%BD%95)
* [学习使我快乐](#%E5%AD%A6%E4%B9%A0%E4%BD%BF%E6%88%91%E5%BF%AB%E4%B9%90)

# Java 

## Java basics

 ### 集合框架

* [ArrayList  学习笔记(jdk1.8)](study-notes/java/java-basics/list/ArrayListStudyNotes.md)  

* [LinkedList 学习笔记(jdk1.8)](study-notes/java/java-basics/list/LinkedListStudyNotes.md)  

### io

* [java io 学习笔记](study-notes/java/java-basics/io/io.md)
  * [NIO 学习笔记](study-notes/java/java-basics/io/nio.md)

### 类加载器

* [类加载器](study-notes/java/java-basics/classLoader/classLoader.md)

  

## Java 并发

* [java 并发学习笔记](study-notes/java/java-concurrency/java-concurrency.md)
  * [cas](study-notes/java/java-concurrency/cas.md)
  * [Java 并发机制的底层实现原理](study-notes/java/java-concurrency/Java-concurrency-underlying-implementation-principle.md)
  * [偏向锁、轻量级锁、重量级锁](study-notes/java/java-concurrency/upgrade-of-lock.md)
* [Java 内存模型](study-notes/java/java-concurrency/jmm/jmm.md)
  * [java 内存模型的基础](study-notes/java/java-concurrency/jmm/jmm-basics.md)

# 开源项目学习笔记

## Tomcat 学习笔记

* [tomcat 学习笔记](study-notes/java/tomcat/tomcat.md)
  * [NioEndpoint 组件](study-notes/java/tomcat/NioEndPoint-model.md)

​        

# 学习站点记录

 [学习资料](./study-notes/learning-material.md)

# 学习使我快乐
<img src="https://raw.githubusercontent.com/caofangqi/leaves-study/master/img/leaves-study.png"/>

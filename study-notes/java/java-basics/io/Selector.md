# Selector

Selector（选择器）是Java NIO中能够检测一到多个NIO通道，并能够知晓通道是否为诸如读写事件做好准备的组件。这样，一个单独的线程可以管理多个channel，从而管理多个网络连接。



## 为什么使用 Selector?

仅用单个线程来处理多个Channels的好处是，只需要更少的线程来处理通道。事实上，可以只用一个线程处理所有的通道。对于操作系统来说，线程之间上下文切换的开销很大，而且每个线程都要占用系统的一些资源（如内存）。因此，使用的线程越少越好。

下面是单线程使用一个Selector处理3个channel的示例图：

![](https://raw.githubusercontent.com/caofangqi/leaves-study/master/img/java-basics/io/nio-selectors.png)









# 参考资料

* [Java NIO系列教程（六） Selector](http://ifeve.com/selectors/)
* [Java NIO Selector](http://tutorials.jenkov.com/java-nio/selectors.html)

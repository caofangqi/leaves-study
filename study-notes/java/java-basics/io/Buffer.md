[TOC]

# Buffer

Java NIO中的 Buffer 用于和 NIO 通道(Channel)进行交互。数据是从通道读入缓冲区，从缓冲区写入到通道中的。[Channel 详细](./Channel.md)

**缓冲区本质上是一块可以写入数据，然后可以从中读取数据的内存。这块内存被包装成NIO Buffer对象，并提供了一组方法，用来方便的访问该块内存。**

## Buffer 的基本用法

使用 Buffer 读写数据一般遵循以下四个步骤:

1. 写入数据到 Buffer 
2. 调用 flip() 方法
3. 从 Buffer 中读取数据
4. 调用 clear() 方法清空数据或者 compact() 方法清楚已经读过的数据。

当向 Buffer 写入数据时，Buffer 会记录下写了多少数据。一旦要读取数据，需要通过 flip() 方法将 Buffer 从写模式切换到读模式。在读模式下，可以读取之前写入到 Buffer 的所有数据。

一旦读完了所有的数据，就需要清空缓冲区，让它可以再次被写入。有两种方式清空缓冲区:

* clear() 方法

  clear() 方法会清空整个缓冲区。

* compact() 方法

  compact() 方法只会清除已经读过的数据。任何未读的数据都被移到缓冲区的起始处，新写入的数据将放到缓冲区未读数据的后面。



[Buffer 使用Demo](https://github.com/caofangqi/leaves-study/blob/master/java/study-java-basics/src/main/java/com/caofangqi/study/io/nio/NioTest.java)

```java

  public static void clearBufferDemo() {
    try (
    // 创建 RandomAccessFile 实例 love.txt:文件地址，rw: 表示读写权限
    // 详见 https://blog.csdn.net/weixin_39913200/article/details/85228072
    RandomAccessFile raf = new RandomAccessFile("love.txt", "rw")) {
      // 获取文件通道
      FileChannel fc = raf.getChannel();
      // 创建 8 个字节容量的缓冲区
      ByteBuffer byteBuffer = ByteBuffer.allocate(8);
      // 读取通道中的数据写入到 缓冲区
      int bytesRead = fc.read(byteBuffer);
      while (bytesRead != -1) {
        // 翻转 准备读取缓冲区
        byteBuffer.flip();
        // 判断缓冲区 是否还有下一个字节
        while (byteBuffer.hasRemaining()) {
          // 从 缓冲区 中读取数据 转成字符打印
          System.out.print((char) byteBuffer.get());
        }
        // 清除数据(并没有删除数据，只是重置了标记，后面写入数据的时候会覆盖以前的数据) 准备重新写入
        byteBuffer.clear();
        // 读取通道中的数据写入到 缓冲区
        bytesRead = fc.read(byteBuffer);
      }

    } catch (Exception e) {
      e.printStackTrace();
    }
  }
```



## Buffer 的 capacity , position 和 limit

缓冲区本质上是一块可以写入数据，然后可以从中读取数据的内存。这块内存被包装成NIO Buffer对象，并提供了一组方法，用来方便的访问该块内存。

Buffer 内部包装了一个字节数组，用以存储缓冲区数据。它通过三个属性保存数据的位置以及状态:

![](https://raw.githubusercontent.com/caofangqi/leaves-study/master/img/java-basics/io/buffers-modes.png)

* capacity

  缓冲区数组的长度，也就是缓冲区的容量

* position

  当你写数据到 Buffer 中时，position 表示当前的位置。初始的 position值为 0.当一个 byte、long 等数据写到 Buffer 后， position 会向前移动到下一个可插入数据的 Buffer 单元。position  最大可为 capacity – 1

  当读取数据时，也是从某个特定位置读。当将Buffer从写模式切换到读模式，position会被重置为0. 当从Buffer的position处读取数据时，position向前移动到下一个可读的位置。

* limit

  在写模式下，Buffer的limit表示你最多能往Buffer里写多少数据。 写模式下，limit等于Buffer的capacity。

  当切换Buffer到读模式时， limit表示你最多能读到多少数据。因此，当切换Buffer到读模式时，limit会被设置成写模式下的position值。换句话说，你能读到之前写入的所有数据（limit被设置成已写数据的数量，这个值在写模式下就是position）

  limit <= capacity



## Buffer 的类型

Buffer 主要实现如下:

- ByteBuffer

  对应基本数据类型 byte

- CharBuffer

  对应基本数据类型 char

- DoubleBuffer

  对应基本数据类型 double

- FloatBuffer

  对应基本数据类型 float 

- IntBuffer

  对应基本数据类型 int 

- LongBuffer

  对应基本数据类型 long

- ShortBuffer

  对应基本数据类型 short

- MappedByteBuffer



## 分配一个缓冲区 Buffer 

要想获得一个Buffer对象首先要进行分配。 每一个Buffer类都有一个allocate方法。下面是一个分配48字节capacity的ByteBuffer的例子。

```java
ByteBuffer byteBuffer = ByteBuffer.allocate(48);
```

分配一个可以存储 1024个字符的 CharBuffer:

```java
CharBuffercharbuffer = CharBuffer.allocate(1024);
```



## 写入数据到 Buffer

有两种方法可以写入数据到  Buffer:

1. 从 Channel 写入数据到 Buffer

   ```java
   int bytesRead = inChannel.read(buf); //read into buffer.
   ```

2. 通过 Buffer 的 put() 方法写到 Buffer里

   ```java
   byteBuffer.put(127);    
   ```

   put 方法有多种版本，允许以不同的方式把数据写入到 Buffer 中。列如写入到一个指定的位置，或者把一个字节数据写入到 Buffer。



## flip() 方法

flip方法将Buffer从写模式切换到读模式。调用flip()方法会将position设回0，并将limit设置成之前position的值。

换句话说， position 现在用于标记读的位置，limit表示之前写进了多少个byte、char等 ，现在能读取多少个byte、char等。



## 从 Buffer 中读取数据

从 Buffer  中读取数据有两种方式:

1. 从 Buffer 中读取数据到 Channel

   ```java
   //read from buffer into channel.
   int bytesWritten = inChannel.write(buf);
   ```

2. 使用 get() 方法从 Buffer 中读取数据

   ```java
   byte aByte = byteBuffer.get();
   ```

get 方法有很多版本，允许不同的方式从Buffer中读取数据。例如，从指定position读取，或者从Buffer中读取数据到字节数组。



## rewind() 方法

Buffer.rewind()将position设回0，所以你可以重读Buffer中的所有数据。limit保持不变，仍然表示能从Buffer中读取多少个元素（byte、char等）。

[rewind() Demo](https://github.com/caofangqi/leaves-study/blob/master/java/study-java-basics/src/main/java/com/caofangqi/study/io/nio/NioBufferTest.java)

## clear()与compact()方法

一旦读完Buffer中的数据，需要让Buffer准备好再次被写入。可以通过clear()或compact()方法来完成。

* clear() 方法

  调用 clear() 方法,position将被设回0，limit被设置成 capacity的值。换句话说，Buffer 被清空了。Buffer中的数据并未清除，只是这些标记告诉我们可以从哪里开始往Buffer里写数据。

* compact() 方法

  compact()方法将所有未读的数据拷贝到Buffer起始处。然后将position设到最后一个未读元素正后面。limit属性依然像clear()方法一样，设置成capacity。现在Buffer准备好写数据了，但是不会覆盖未读的数据。

## mark() 与 reset() 方法

通过调用Buffer.mark()方法，可以标记Buffer中的一个特定position。之后可以通过调用Buffer.reset()方法恢复到这个position。例如：

```java
buffer.mark();
//call buffer.get() a couple of times, e.g. during parsing.
buffer.reset();  //set position back to mark.

```

 [mark() 与 reset() Demo](https://github.com/caofangqi/leaves-study/blob/master/java/study-java-basics/src/main/java/com/caofangqi/study/io/nio/NioBufferTest.java)

# 参考资料

* [Java NIO Buffer](http://tutorials.jenkov.com/java-nio/buffers.html#capacity-position-limit)
* [Java NIO系列教程（三） Buffer](http://ifeve.com/buffers/)
* [攻破JAVA NIO技术壁垒](https://blog.csdn.net/u013256816/article/details/51457215)


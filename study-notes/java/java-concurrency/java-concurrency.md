[TOC]

# Java 并发

读  java 并发编程的艺术(方腾飞，魏鹏，程晓明 著)  记录的一些笔记。

## 并发编程的挑战

[并发编程的挑战](https://github.com/caofangqi/leaves-study/blob/master/study-notes/java/java-concurrency/java-concurrency-challenge.md)

## Java 并发机制的底层实现原理

[Java 并发机制的底层实现原理](./Java-concurrency-underlying-implementation-principle.md)

## Java 内存模型 JMM

[JMM](https://github.com/caofangqi/leaves-study/blob/master/study-notes/java/java-concurrency/jmm/jmm.md)

# 参考资料

* java 并发编程的艺术


# CAS ==> Compare and Swap

## 什么是 CAS ？

compare and swap 比较并交换。

CAS 操作需要输入两个值，一个旧值 (操作之前的值) 和一个新值，在操作期间先比较旧值有没有发生变化，如果没有发生变化，才交换成新值，发生了变化则不交换。



## CAS 是怎么实现的

JVM 中的CAS 操作是利用了处理器提供的 CMPXCHG 指令实现的。

交换指令 CMPXCHG，被该指令操作的内存区域就会加锁，导致其他处理器不能同时访问它。

详情请见 [处理器如何实现原子操作](https://github.com/caofangqi/leaves-study/blob/master/study-notes/java/java-concurrency/Java-concurrency-underlying-implementation-principle.md)

自旋锁 CAS 实现的基本思路就是循环进行 CAS 操作直到成功为止。

由此可见 CAS 其实也是一个乐观锁。

## java 中使用 CAS 实现原子操作

[代码在此](https://github.com/caofangqi/leaves-study/blob/master/demo/java/study-java-concurrency/src/main/java/com/caofangqi/study/concurrency/cas/CasCounter.java)：

```java
public class CasCounter {
  private int i = 0;
  private AtomicInteger atomicInteger = new AtomicInteger(0);

  /** 安全的计数方法 */
  public void safeCount() {
    for (; ; ) {
      boolean suc = atomicInteger.compareAndSet(atomicInteger.get(), atomicInteger.get() + 1);
      if (suc) {
        break;
      }
    }
  }
  /** 不安全的计数方法 */
  public void unSafeCount() {
    i++;
  }

  public static void main(String[] args) {
    List<Thread> list = new ArrayList<>(500);
    CasCounter cc = new CasCounter();
    for (int i = 0; i < 500; i++) {
      list.add(
          new Thread(
              new Runnable() {
                @Override
                public void run() {
                  for (int j = 0; j < 100; j++) {
                    cc.safeCount();
                    cc.unSafeCount();
                  }
                }
              }));
    }

    for (Thread t : list) {
      t.start();
    }

    list.forEach(
        t -> {
          try {
            t.join();
          } catch (InterruptedException e) {
            e.printStackTrace();
          }
        });

    System.out.println("i 的值:" + cc.i);
    System.out.println("atomicInteger 的值:" + cc.atomicInteger);
  }
}
```

执行上面代码中的 main 方法，结果如下：

```txt
i 的值:49944
atomicInteger 的值:50000
```

i 的值每次运行都不一样，而 atomicInteger 的值一直是 正确的 50000 。

## CAS 实现原子操作的三大问题

CAS 虽然很高效地解决了原子操作，但是 CAS 仍然存在三大问题。

1. ABA 问题。

   CAS 在操作值的时候会去检查 旧值有没有变化，但是如果 一个值原来是 A，变成了B，又变成了A，那么 使用CAS 检查时就会发现旧值没有发生变化，但是实际上却变化了。 

   ABA 问题的解决思路是使用版本号，在变量前面追加上版本号，每次变量改变的时候给版本号加1，那么  A -> B -> A  就会变成 1A -> 2B -> 3A 。

   从 java 1.5 开始，JDK 的 Atomic 包里提供了一个类 AtomicStampedReference 来解决 ABA问题。 这个类的 compareAndSet 方法 首先会检查当前引用是否等于预期引用，并且检查当前标志是否等于预期标志，如果全部相等，则以原子方式将该引用和该标志的值设定为给定的更新值。

2. 循环时间长开销大。

   自旋 CAS 如果长时间不成功，会给 CPU 带来非常大的执行开销。

3. 只能保证一个共享变量的原子操作。

   当对一个共享变量执行操作时，我们可以使用循环 CAS 的方式来保证原子操作，但是对于多个共享变量操作时，循环 CAS 就无法保证操作的原子性。从 java 1.5 开始，jdk 提供了 AtomicReference 类来保证引用对象之间的原子性，就可以把多个变量放在一个对象里来进行 CAS 操作。

# 参考资料

* [JAVA 中的 CAS](https://juejin.im/post/5a75db20f265da4e826320a9)

* Java 并发编程的艺术

  


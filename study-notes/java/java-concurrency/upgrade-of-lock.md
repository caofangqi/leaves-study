# 偏向锁、轻量级锁 、重量级锁

JAVA  SE 1.6 为了减少获得锁和释放锁带来的性能消耗，引入了 "偏向锁" 和 "轻量级锁"。

在 java 1.6 中，锁一共有 4 种状态，级别从低到高依次是： 无锁状态、偏向锁状态、轻量级锁状态 和重量级锁状态。

这几种状态会随着竞争情况逐渐升级，锁可以升级但不能降级，意味着偏向锁升级成轻量级锁后不能降级成偏向锁。这种锁升级却不能降级的策略，目的是为了提高获得锁和释放锁的效率。



## [Synchronized 的底层实现](./java-concurrency-underlying-implementation-principle.md)



### 锁是存在哪里的，怎么存储的？

**synchronized 用的锁是存在 java 对象头里的。对象头包括两部分信息，第一部分存储 对象自身的运行时数据(哈希码(HashCode) 、GC分代年龄、锁信息)，这部分叫 Mark Word.** 这部分数据的长度在 32 位虚拟机下和 64 位虚拟机下分别是 32 和 64. 第二部分 存储的是类型指针，即对象指向它元数据的指针，虚拟机通过这个指针来确定这个对象是哪个类的实例，这部分叫  Klass Word.

在运行期间， Mark Word 里存储的数据会随着锁标志位的变化而变化。Mark Word 在对象头中的存储结构如下所示：

![](https://raw.githubusercontent.com/caofangqi/leaves-study/master/img/jvm/java-object-headers-32.png)

![](https://raw.githubusercontent.com/caofangqi/leaves-study/master/img/jvm/java-object-headers-64-compressed-oops.png)

![](https://raw.githubusercontent.com/caofangqi/leaves-study/master/img/jvm/java-object-headers-64.png)





### 偏向锁

**经过研究发现，大多数情况下，锁不仅不存在多线程竞争，而且总是由同一线程多次获得，为了让线程获得锁的代价更低而引入了偏向锁。**

当一个线程访问同步块并获取锁时，会在**对象头和栈帧中的锁记录里存储偏向的线程ID**，以后该线程在**进入和退出同步块时不需要进行 CAS 操作来加锁和解锁**，只需简单的测试一下对象头的 Mark Word 里是否存储着指向当前线程的偏向锁。 如果测试成功，表示线程已经获得了锁。如果测试失败，则需要再测试一下 Mark Word 中偏向锁的标识是否设置成了1(标识当前是偏向锁):如果没有设置，则使用 CAS 竞争锁，如果设置了，则尝试使用 CAS 将对象头的偏向锁指向当前线程。

**偏向锁使用了等到竞争出现才释放锁的机制，所以当其他线程尝试竞争偏向锁时，持有偏向锁的线程才会释放锁。**

偏向锁的撤销，需要等待全局安全点(在这个时间点上没有正在执行的字节码)。它会首先暂停拥有偏向锁的线程，然后检查持有偏向锁的线程是否有活着，如果线程不处于活动状态，则将对象头设置成无锁状态，如果线程仍然活着，拥有偏向锁的栈会被执行，遍历偏向对象的锁记录，栈中的锁记录和对象头的  Mark Word   要么重新偏向于其他线程，要么恢复到无锁或者标记对象不适合作为偏向锁，最后唤醒暂停的线程。



简单的说：

 锁偏向线程1，线程1还活着，线程2尝试获取锁，此时锁升级为轻量级锁。



  锁偏向线程1，线程1 已经死亡，线程2 尝试获取锁，设置偏向锁标识为0(线程1执行完毕后不会主动释放锁)，使用 CAS 替换偏向锁线程 ID 为线程2.



如下图所示，偏向锁的获得和撤销流程(图来源自网络):

![](https://raw.githubusercontent.com/caofangqi/leaves-study/master/img/java-concurrency/biased-lock.png)

#### 关闭偏向锁

````
-XX:-UseBiasedLocking=false //关闭偏向锁
-XX:BiasedLockingStartupDelay=0 //偏向锁在 java6 和 java7 里是默认启用的，但是它在应用程序启动几秒钟之后才激活，此参数用来关闭这个延迟。
````



### 轻量级锁

#### 轻量级锁加锁

#### 轻量级锁加锁

线程在执行同步块之前，JVM 会先在当前线程的栈帧中创建用于存储锁记录的空间，并将对象头中的 Mark Word 复制到锁记录中，官方称为 Displaced Mark Word 。

然后线程尝试使用 CAS 将对象头中的 Mark Word 替换为指向锁记录的指针。如果成功，当前线程获得锁，如果失败表示其他线程竞争锁，当前线程便尝试使用自旋来获取锁，当自旋达到一定次数之后未获得锁，便会膨胀成重量级锁。

#### 轻量级锁解锁

轻量级锁解锁时，会使用原子的 CAS 操作将 Displaced Mark  Word 替换回对象头，如果成功，则表示没有竞争发生。如果失败，表示当前锁存在竞争，锁就会膨胀成重量级锁。

因为自旋会消耗 CPU ，为了避免无用的自旋(比如获得锁的线程被阻塞住了)，一旦锁升级成重量级锁，就不会再恢复到轻量级锁状态。

轻量级锁及膨胀流程图(图来源于网络):

![](https://raw.githubusercontent.com/caofangqi/leaves-study/master/img/java-concurrency/light-weight-lock.png)



### 重量级锁

当锁处于重量级锁状态下，其他线程获取锁时，都会被阻塞住，当持有锁的线程释放锁之后会唤醒这些线程，被唤醒的线程就会进行新一轮的夺锁之争。



## 锁的优缺点对比

| 锁       | 优点                                                         | 缺点                                             | 适用场景                               |
| -------- | ------------------------------------------------------------ | ------------------------------------------------ | -------------------------------------- |
| 偏向锁   | 加锁和解锁不需要额外的消耗，和执行非同步方法相比仅存在纳秒级的差距 | 如果线程间存在锁竞争，会带来额外的锁撤销的消耗   | 适用于只有一个线程访问同步块场景       |
| 轻量级锁 | 竞争的线程不会阻塞，提高了程序的响应速度                     | 如果始终得不到锁，竞争锁的线程使用自旋会消耗 CPU | 追求响应时间，同步代码块执行速度非常快 |
|          | 线程竞争不需要自旋，不会消耗 CPU                             | 线程阻塞，响应时间慢                             | 追求吞吐量，同步代码块执行速度较长     |



synchronized 锁机制原理图(图来源于网络):

![](https://raw.githubusercontent.com/caofangqi/leaves-study/master/img/java-concurrency/synchronized.png)



# 参考资料

- Java 并发编程的艺术 -方腾飞，魏鹏，程晓明 著
- java 虚拟机规范(Java SE 8 版) 
- [Java并发之彻底搞懂偏向锁升级为轻量级锁](https://www.cnblogs.com/tiancai/p/9382542.html)
- [ObjectHeaders](https://gist.github.com/arturmkrtchyan/43d6135e8a15798cc46c)
- https://stackoverflow.com/questions/26357186/what-is-in-java-object-header
- https://wiki.openjdk.java.net/display/HotSpot/Synchronization
- https://www.jianshu.com/p/c5058b6fe8e5
- https://juejin.im/post/5bfe6eafe51d4524f35d04d1
- [【java并发编程实战4】偏向锁-轻量锁-重量锁的那点秘密(synchronize实现原理)](https://www.jianshu.com/p/3aac4239a84c)




# Java 并发机制的底层实现原理

[TOC]

## volatile

**volatile 是轻量级的synchronized,它在多处理器开发中保证了共享变量的 可见性 。**

```
 可见性 : 当一个线程修改一个共享变量时，另外一个线程能读到这个修改的值。
```

volatile 比 synchronized 的使用和执行成本更低，因为它不会引起线程上下文的切换和调度。

### volatile 的定义与实现原理

**如果一个字段被声明成 volatile ，Java 线程内存模型确保所有线程看到的这个变量的值是一致的。**



volatile 关键字修饰的共享变量进行 写操作的时候 JVM 会向处理器发送一条 Lock 前缀的指令，Lock指令在多核处理器下会引发两件事情：

1. 将当前处理器缓存行的数据写回到系统内存。
2. 这个写回内存的操作会使在其他 CPU 里缓存了该内存地址的数据无效。



为了提高速度，处理器不直接与内存进行通信，而是先将系统内存的数据读到内部缓存(L1,L2 或其他)后再进行操作，但操作完不知道何时会写到内存。

CPU多级缓存如下图所示（[图来源于网络](http://www.woshipm.com/pd/425699.html?utm_source=gold_browser_extension)）:

![](<https://raw.githubusercontent.com/caofangqi/leaves-study/master/img/java-concurrency/cpu-cache.png>)

如果对声明了 volatile 的变量进行写操作，JVM 就会向处理器发送一条 Lock 前缀的指令，将这个变量所在缓存行的数据写回到系统内存。但是，就算写回到系统内存，如果其他处理器缓存的值还是旧值，再执行计算操作就会有问题。

**所以在多处理器下，为了保证各个处理器的缓存是一致的，就会实现缓存一致性协议**，每个处理器通过嗅探在总线上传播的数据来检查自己缓存的值是不是过期了，当处理器发现自己缓存对应得内存地址被修改，就会将当前处理器的缓存行设置成无效状态，当处理器对这个数据进行修改操作的时候，会重新从系统内存中把数据读到处理器缓存里。

缓存一致性协议图([图来源于网络](https://blog.csdn.net/n9nzjx57bf/article/details/71638893))：

![](https://raw.githubusercontent.com/caofangqi/leaves-study/master/img/java-concurrency/cpu-cache-consistency.jpg)



volatile 的两条实现原则：

1. Lock 前缀指令会引起处理器缓存回写到内存。
2. 一个处理器的缓存回写到内存会导致其他处理器的缓存无效。



## synchronized

java 1.6 对 synchronized 进行了各种优化，为了减少获得锁和释放锁带来的性能消耗而引入偏向锁和轻量级锁。

synchronized 实现同步的基础: Java 中的每一个对象都可以作为锁。具体有三种形式:

* 对于普通同步方法，锁是当前实例对象。
* 对于静态同步方法，锁是当前类的 Class 对象。
* 对于同步方法块，锁是 Synchonized 括号里配置的对象。

### synchronized 实现的底层原理

**java 虚拟机可以支持方法级别的同步，和方法内部一段指令序列 (代码块) 的同步,这两种同步结构都是使用同步锁(monitor)来支持的。**

#### 方法级同步

方法级同步是隐式的，即无需通过字节码指令来控制，它实现在方法调用和返回操作之中。

**隐式同步 (方法级同步) 依赖方法调用和返回指令实现。**

方法级同步演示方法:

```java
  public synchronized void methodSyncTest() {
    System.out.println("hello,world");
  }
```

通过命令 ` javap -c -v -l -s  class文件名`   查看编译后的指令：

 ![](https://raw.githubusercontent.com/caofangqi/leaves-study/master/img/java-concurrency/synchronized-method.PNG)



虚拟机可以从方法常量池中的方法表结构 (method_info structure)中的 **ACC_SYNCHRONIZED** 访问标志区分一个方法是否是同步方法。如上图所示，当**调用方法时，调用指令将会检查方法的 ACC_SYNCHRONIZED 访问标志是否设置，如果设置了，执行线程将先持有同步锁，然后执行方法，最后在方法完成 (无论是正常完成还是非正常完成)时释放同步锁。**

**在方法执行期间，执行线程持有了同步锁，其他任何线程都无法再获得同一个锁。**

**如果同步方法执行期间抛出了异常，并且在方法内部无法处理此异常，那这个同步方法所持有的锁将在异常抛到同步方法之外时自动释放。**

#### 代码块同步 (指令序列同步)

代码块同步是显示的，有明确的 monitorenter 和 monitorexit 指令。

 monitorenter 和 monitorexit 指令用于编译同步代码块,实例如下:

```java
  public void blockTest() {
    synchronized (this) {
      System.out.println("hello,world");
    }
  }
```

通过命令 ` javap -c -v -l -s  class文件名`   查看编译后的指令：

 ![](https://raw.githubusercontent.com/caofangqi/leaves-study/master/img/java-concurrency/synchronized-code-block.PNG)

从上图可以看出，synchronized 同步代码块使用的是 monitorenter 和 monitorexit 指令.

**编译器必须确保无论方法以何种方式完成，方法中调用过的每条 monitorenter 指令都必须有对应的 monitorexit 指令得以执行，不管这个方法是正常结束还是异常结束都应如此。**

为了保证在方法异常完成时， monitorenter 和 monitorexit 指令依然可以正确配对执行，编译器会自动产生一个异常处理器，这个异常处理器宣称自己可以处理所有的异常，它的代码用来执行 monitorexit 指令。

 [以上 demo 链接.](https://github.com/caofangqi/leaves-study/blob/master/java/study-java-concurrency/src/main/java/com/caofangqi/study/concurrency/sync/SynchronizedTest.java)



#### 偏向锁，轻量级锁，重量级锁

 [我是萌萌哒的链接](./upgrade-of-lock.md)

synchronized 锁机制原理图(图来源于网络):

![](https://raw.githubusercontent.com/caofangqi/leaves-study/master/img/java-concurrency/synchronized.png)



## 原子操作的实现原理

### 概念:

* 原子  atomic  

    不能被进一步分割的最小粒子

* 原子操作 atomic operation)

   不可被中断的一个或一系列操作

### 处理器如何实现原子操作

1. 使用总线锁保证原子性。

   如果多个处理器同时对共享变量进行读改写操作 (例如:i++),那么共享变量就会被多个处理器同时进行操作，这样读改写操作就不是原子的，操作完之后共享变量的值会和期望的不一致。不一致的原因可能是因为多个处理器同时从各自的缓存中读取变量 i,分别进行 +1操作，然后分别写入系统内存中。

   想要保证读改写共享变量的操作是原子的，就必须保证其中一个 CPU 读改写共享变量的时候，其他 CPU 不能操作该共享变量的缓存。

   处理器的总线锁就是用来解决这个问题的，**总线锁就是使用处理器提供的一个 LOCK # 信号，当一个处理器在总线上输出此信号时，其他处理器的请求将被阻塞住，那么该处理器可以独占共享内存。**

2. 使用缓存锁保证原子性。

   在同一时刻内，我们只需要保证对某个内存地址的操作是原子性即可，但总线锁定把 CPU 和 内存之间的通信锁住了，这使得锁定期间，其他处理器不能操作其他内存地址的数据，所以总线锁定的开销比较大，目前处理器在某些场合下使用缓存锁定代替总线锁定来优化。

   频繁使用的内存会缓存在处理器的 L1，L2 和 L3 高速缓存里，那么原子操作就可以直接在处理器内部缓存中进行，并不需要声明总线锁。

   **缓存锁定是指内存区域如果被缓存在处理器的缓存行中，并且在 Lock 操作期间被锁定，那么当它执行锁操作回写到内存时，处理器不在总线上声言 LOCK # 信号，而是修改内部的内存地址，并允许它的缓存一致性机制来保证操作的原子性。**

   因为缓存一致性机制会阻止同时修改由两个以上处理器缓存的内存区域数据，当其他处理器回写已被锁定的缓存行的数据时，会使缓存行无效。

   **但是有两种情况下处理器不会使用缓存锁定:**

   * 当操作的数据不能被缓存在处理器内部，或操作的数据跨多个缓存行(cache line)时，则处理器会调用总线锁定。
   * 有些处理器不支持缓存锁定。

   

   

   对于以上两种机制，我们通过 Intel  处理器提供了很多  Lock 前缀的指令来实现。
   例如：位测试和修改指令：BTS、BTR、BTC；交换指令 XADD 、CMPXCHG，以及其他一些操作数和逻辑指令(如 ADD、OR)等，被这些指令操作的内存区域就会加锁，导致其他处理器不能同时访问它。

 

### java 中如何实现原子操作

在 java 中可以通过锁和循环 CAS 的方式来实现原子操作。

1. 使用循环 CAS 实现原子操作

   [CAS 笔记](https://github.com/caofangqi/leaves-study/blob/master/study-notes/java/java-concurrency/cas.md)

2. 使用锁机制实现原子操作

   锁机制保证了只有获得锁的线程才能操作锁定的内存区域。



# 参考资料

* Java 并发编程的艺术 -方腾飞，魏鹏，程晓明 著
* java 虚拟机规范(Java SE 8 版) 
* [Java并发之彻底搞懂偏向锁升级为轻量级锁](https://www.cnblogs.com/tiancai/p/9382542.html)

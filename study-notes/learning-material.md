# 学习资料收集

记录一下学习站点。

## java

1. [Java Development Kit builds, from Oracle](<https://jdk.java.net/>)

   这里有 Oracle JDK 源码

2. [OpenJDK](<http://openjdk.java.net/>)

   开源 JDK 所在地

3. https://wiki.openjdk.java.net/

   OpenJDK Wiki

4. [JAVA 实现的设计模式](https://java-design-patterns.com/)





## 学习站点

1. [并发编程网 - ifeve.com](http://ifeve.com/)

    并发编程网，大量翻译原创精品文章

2. [掘金](https://juejin.im/timeline)

3. [IBM 技术博客](https://www.ibm.com/developerworks/cn/topics/)

4. [Software Development & Entrepreneurship Tutorials](http://tutorials.jenkov.com/)

5. [Java code geeks](https://www.javacodegeeks.com/)

6. [Java Tutorials](https://howtodoinjava.com/)

7. [Hollis 的个人网站](http://www.hollischuang.com/)

8. [Java 技术驿站](http://cmsblogs.com/)

    

    

    

    

    

    

    

    

   

​      




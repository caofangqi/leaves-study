package com.caofangqi.study.studytomcat.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
/**
 * 测试 2019年7月2日13:52:38
 *
 * @author 叶子
 */
@RestController
public class TestController {
  @RequestMapping("/test")
  public String test() {

    return "hello,world";
  }
}

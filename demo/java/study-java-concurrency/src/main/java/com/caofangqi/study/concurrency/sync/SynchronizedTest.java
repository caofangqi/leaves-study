package com.caofangqi.study.concurrency.sync;
/**
 * Synchronized 测试 javap -c -v -l -s SynchronizedTest.class 反编译
 *
 * @author 叶子 2019/06/19 23:04:02
 */
public class SynchronizedTest {

  public void blockTest() {
    synchronized (this) {
      System.out.println("hello,world");
    }
  }

  public synchronized void methodSyncTest() {
    System.out.println("hello,world");
  }
}

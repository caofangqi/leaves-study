package com.caofangqi.study.concurrency.cas;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * 使用 CAS 实现计数器
 *
 * @author 叶子 2019/06/27 22:52:39
 */
public class CasCounter {
  private int i = 0;
  private AtomicInteger atomicInteger = new AtomicInteger(0);

  /** 安全的计数方法 */
  public void safeCount() {
    for (; ; ) {
      boolean suc = atomicInteger.compareAndSet(atomicInteger.get(), atomicInteger.get() + 1);
      if (suc) {
        break;
      }
    }
  }
  /** 不安全的计数方法 */
  public void unSafeCount() {
    i++;
  }

  public static void main(String[] args) {
    List<Thread> list = new ArrayList<>(500);
    CasCounter cc = new CasCounter();
    for (int i = 0; i < 500; i++) {
      list.add(
          new Thread(
              new Runnable() {
                @Override
                public void run() {
                  for (int j = 0; j < 100; j++) {
                    cc.safeCount();
                    cc.unSafeCount();
                  }
                }
              }));
    }

    for (Thread t : list) {
      t.start();
    }

    list.forEach(
        t -> {
          try {
            t.join();
          } catch (InterruptedException e) {
            e.printStackTrace();
          }
        });

    System.out.println("i 的值:" + cc.i);
    System.out.println("atomicInteger 的值:" + cc.atomicInteger);
  }
}

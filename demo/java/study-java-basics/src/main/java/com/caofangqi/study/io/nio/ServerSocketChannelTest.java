package com.caofangqi.study.io.nio;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Iterator;

/**
 * NIO 方式的服务端
 *
 * @date 2019/06/19 16:48:20
 */
public class ServerSocketChannelTest {

  private static final int BUF_SIZE = 1024;
  /**
   * 启动NIO 服务端监听
   *
   * @date 2019/06/20 14:16:10
   */
  public static void listen(int port) {
    Selector selector = null;
    ServerSocketChannel ssc = null;

    try {
      // 创建 Selector
      selector = Selector.open();
      // 创建 ServerSocketChannel
      ssc = ServerSocketChannel.open();
      // 将通道 绑定到一个端口
      ssc.bind(new InetSocketAddress(port));
      // 设置为非阻塞模式
      ssc.configureBlocking(false);
      // 注册到 selector 并且注册 SelectionKey.OP_ACCEPT 事件
      ssc.register(selector, SelectionKey.OP_ACCEPT);
      while (true) {
        if (selector.select() == 0) {
          System.out.println("===== keys 为零");
          continue;
        }
        Iterator<SelectionKey> iter = selector.selectedKeys().iterator();
        while (iter.hasNext()) {
          SelectionKey key = iter.next();
          if (key.isAcceptable()) {
            handleAccept(key);
          }
          if (key.isReadable()) {
            handleRead(key);
          }
          if (key.isWritable() && key.isValid()) {
            handleWrite(key);
          }
          if (key.isConnectable()) {
            System.out.println("isConnectable = true");
          }
          iter.remove();
        }
      }

    } catch (Exception e) {
      e.printStackTrace();
    } finally {
      try {
        if (selector != null) {
          selector.close();
        }
        if (ssc != null) {
          ssc.close();
        }
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
  }

  public static void handleWrite(SelectionKey key) throws IOException {
    ByteBuffer buf = (ByteBuffer) key.attachment();
    buf.flip();
    SocketChannel sc = (SocketChannel) key.channel();
    while (buf.hasRemaining()) {
      sc.write(buf);
    }
    buf.compact();
  }

  public static void handleRead(SelectionKey key) throws IOException {
    SocketChannel sc = (SocketChannel) key.channel();
    ByteBuffer buf = (ByteBuffer) key.attachment();
    long bytesRead = sc.read(buf);
    while (bytesRead > 0) {
      buf.flip();
      while (buf.hasRemaining()) {
        System.out.print((char) buf.get());
      }
      System.out.println();
      buf.clear();
      bytesRead = sc.read(buf);
    }
    if (bytesRead == -1) {
      sc.close();
    }
  }

  public static void handleAccept(SelectionKey key) throws IOException {
    ServerSocketChannel ssChannel = (ServerSocketChannel) key.channel();
    SocketChannel sc = ssChannel.accept();
    sc.configureBlocking(false);
    sc.register(key.selector(), SelectionKey.OP_READ, ByteBuffer.allocateDirect(BUF_SIZE));
  }
}

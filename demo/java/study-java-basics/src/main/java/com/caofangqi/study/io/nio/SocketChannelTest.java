package com.caofangqi.study.io.nio;

import java.io.IOException;
import java.io.InputStream;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.util.concurrent.TimeUnit;

/**
 * SocketChannel Demo
 *
 * @author 叶子
 * @date 2019/06/19 15:19:46
 */
public class SocketChannelTest {

  /** NIO 实现的客户端 */
  public static void clientOfNio(int port) {
    ByteBuffer buffer = ByteBuffer.allocate(1024);
    SocketChannel socketChannel = null;
    try {
      // 打开 SocketChannel
      socketChannel = SocketChannel.open();
      // 设置 SocketChannel 为非阻塞模式（non-blocking mode）
      socketChannel.configureBlocking(false);
      // 连接服务端 SocketChannel在非阻塞模式下，此时调用connect()，该方法可能在连接建立之前就返回了。为了确定连接是否建立，可以调用finishConnect()的方法
      socketChannel.connect(new InetSocketAddress("127.0.0.1", port));

      if (socketChannel.finishConnect()) {
        int i = 0;
        while (true) {

          TimeUnit.SECONDS.sleep(5);
          String info = "I'm " + i++ + "-th information from client";
          buffer.clear();
          buffer.put(info.getBytes());
          buffer.flip();
          while (buffer.hasRemaining()) {
            System.out.println(buffer.toString());
            socketChannel.write(buffer);
          }
        }
      }

    } catch (Exception e) {
      e.printStackTrace();
    } finally {
      try {
        if (socketChannel != null) {
          socketChannel.close();
        }
      } catch (Exception e) {
        e.printStackTrace();
      }
    }
  }

  public static void server(int port) {
    ServerSocket serverSocket = null;
    InputStream is = null;
    try {
      serverSocket = new ServerSocket(port);
      int recvMsgSize = 0;
      byte[] recvBuf = new byte[1024];
      while (true) {
        Socket clntSocket = serverSocket.accept();
        SocketAddress clientAddress = clntSocket.getRemoteSocketAddress();
        System.out.println("Handling client at " + clientAddress);
        is = clntSocket.getInputStream();
        byte[] temp = null;
        while ((recvMsgSize = is.read(recvBuf)) != -1) {
          temp = new byte[recvMsgSize];
          System.arraycopy(recvBuf, 0, temp, 0, recvMsgSize);
          String tempString = new String(temp);
          System.out.println(tempString);
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
    } finally {
      try {
        if (serverSocket != null) {
          serverSocket.close();
        }
        if (is != null) {
          is.close();
        }

      } catch (IOException e) {
        e.printStackTrace();
      }
    }
  }
}

package com.caofangqi.study.io.nio;

import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

/**
 * NIO Buffer 测试
 *
 * @author 叶子
 * @date 2019/06/19 13:56:38
 */
public class NioBufferTest {

  /**
   * Buffer rewind() 方法测试 Buffer.rewind()将position设回0， 所以你可以重读Buffer中的所有数据。
   * limit保持不变，仍然表示能从Buffer中读取多少个元素（byte、char等）。
   *
   * @date 2019/06/19 13:59:58
   */
  public void rewindTest() {

    try (
    // 创建 RandomAccessFile 实例 love.txt:文件地址，rw: 表示读写权限
    // 详见 https://blog.csdn.net/weixin_39913200/article/details/85228072
    RandomAccessFile raf = new RandomAccessFile("love.txt", "rw")) {
      // 获取文件通道
      FileChannel fc = raf.getChannel();
      // 分配 8 个字节容量的缓冲区
      ByteBuffer byteBuffer = ByteBuffer.allocate((int) fc.size());
      // 读取通道中的数据写入到 缓冲区
      int bytesRead = fc.read(byteBuffer);

      // 翻转 准备读取缓冲区
      byteBuffer.flip();
      // 判断缓冲区 是否还有下一个字节
      while (byteBuffer.hasRemaining()) {
        // 从 缓冲区 中读取数据 转成字符打印
        System.out.print((char) byteBuffer.get());
      }
      // 换行
      System.out.println();
      // 将position设回0 重新读数据
      byteBuffer.rewind();
      // 判断缓冲区 是否还有下一个字节
      while (byteBuffer.hasRemaining()) {
        // 从 缓冲区 中读取数据 转成字符打印
        System.out.print((char) byteBuffer.get());
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
  /**
   * mark 方法和 reset 方法测试
   *
   * @date 2019/06/19 14:14:24
   */
  public void markAndResetTest() {
    try (
    // 创建 RandomAccessFile 实例 love.txt:文件地址，rw: 表示读写权限
    // 详见 https://blog.csdn.net/weixin_39913200/article/details/85228072
    RandomAccessFile raf = new RandomAccessFile("love.txt", "rw")) {
      // 获取文件通道
      FileChannel fc = raf.getChannel();
      // 分配 8 个字节容量的缓冲区
      ByteBuffer byteBuffer = ByteBuffer.allocate((int) fc.size());

      // 读取通道中的数据写入到 缓冲区
      fc.read(byteBuffer);
      // 翻转 准备读取缓冲区
      byteBuffer.flip();
      // 标记 position 此时 position =0
      byteBuffer.mark();
      // 判断缓冲区 是否还有下一个字节
      while (byteBuffer.hasRemaining()) {
        // 从 缓冲区 中读取数据 转成字符打印
        System.out.print((char) byteBuffer.get());
      }
      // 换行
      System.out.println();
      // 重置 position 到 mark 的位置 再次读取
      byteBuffer.reset();
      // 判断缓冲区 是否还有下一个字节
      while (byteBuffer.hasRemaining()) {
        // 从 缓冲区 中读取数据 转成字符打印
        System.out.print((char) byteBuffer.get());
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
}
